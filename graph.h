//Tariq Khandwalla
// Tuesday June 4, 2024
// A05
//purpose: the file contains the class definition and declarations for the praph data structure. it defines the Graph class which includes the necessary methods and private data members to manage the graph. 

#ifndef GRAPH_H
#define GRAPH_H

#include <string>
#include <vector>
#include <iostream>
#include <queue>
#include <unordered_set>
#include <memory>

using namespace std;

class Graph {
public:
    Graph(int size); // Constructor to initialize the graph with given size
    ~Graph(); // Destructor to free up the memory

    void insertVertex(const string& name, const vector<string>& interests);
    void insertEdge(int fromVertex, int toVertex, int years);
    void display() const;
    void findMutualFriends(const string& name) const;
    void breadthFirstTraversal(const string& startName) const; // Extra credit: Breadth-first traversal

private:
    struct EdgeNode {
        int vertexIndex; // Index of the adjacent vertex
        int years; // Duration of the friendship in years
        shared_ptr<EdgeNode> next; // Next edge node

        EdgeNode(int vIndex, int y) : vertexIndex(vIndex), years(y), next(nullptr) {}
    };

    struct Vertex {
        string name; // Name of the person
        vector<string> interests; // Interests of the person
        shared_ptr<EdgeNode> head; // Head pointer to the list of edges

        Vertex() : head(nullptr) {}
    };

    vector<Vertex> adjacencyList; // The adjacency list

    void clear(); // Helper function to clear the adjacency list
    void breadthFirstTraversalHelper(int startIndex, unordered_set<int>& visited) const; // Helper for BFS
    int findVertexIndex(const string& name) const; // Helper to find vertex index by name
};

#endif
